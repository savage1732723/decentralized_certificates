const mongoose = require("mongoose")
const validator = require('validator')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true,'Please tell us your name!'],
    },
    email: {
        type: String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    
    photo:{
        type: String,
        default:'default.jpg',
    },
    role:{
        type: String,
        enum: ['user', 'admin'],
        default: "user",

    },
    password: {
        type: String,
        required: [true, 'Please provide a password!'],
        minlength: 8,
        //password wont be included when we get the users
        select: false,
    },
    passwordConfirm:{
        type:String,
        required:[true,'Please confirm your password'], 
        validate:{
            //this only works on save!!!
            validator: function(el){
                return el === this.password
            },
            message:"Passwords are not the same",
        },
    },
    
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
})

const User = mongoose.model('User',userSchema)
module.exports = User